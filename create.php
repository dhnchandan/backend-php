<?php

require_once './db.php';

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// $request = json_decode(file_get_contents("php://input"));

// $name = $request->name;
// $email = $request->email;
// $phone = $request->phone;
// $address = $request->address;
// $street = $request->street;
// $city = $request->city;
// $country = $request->country;
// $image = $request->image;

$name = $_POST['name'];
$email = $_POST['email'];
$phone = $_POST['phone'];
$address = $_POST['address'];
$street = $_POST['street'];
$city = $_POST['city'];
$country = $_POST['country'];
$image = $_FILES['image'];

$imageName = $image['name'];
$imageTmpName = $image['tmp_name'];
$imageType = $image['type'];
$imageSize = $image['size'];
$imageError = $image['error'];

$imageExt = explode('.', $imageName);
$imageActualExt = strtolower(end($imageExt));
$allowed = array('jpg', 'jpeg', 'png');


$fetchQuery = "SELECT * from user";
$fetchResult = mysqli_query($connectResult, $fetchQuery);
$users = array();

while ($row = mysqli_fetch_assoc($fetchResult)) {
	$users[] = $row;
}

for ($i = 0; $i < count($users); $i++) {
	if ($users[$i]['email'] == $email) {
		die("User Already Exists");
	}
}

if (in_array($imageActualExt, $allowed)) {
	if ($imageError == 0) {
		if ($imageSize < 300000) {
			$imageNewName = "profile" . time() . "." . $imageActualExt;
			$imageDestination = 'images/' . $imageNewName;


			$insertQuery = "INSERT INTO user (name, email, phone, address, street, city, country, image) VALUES('$name', '$email', '$phone', '$address', '$street', '$city', '$country', '$imageDestination')";

			$insertResult = mysqli_query($connectResult, $insertQuery);

			move_uploaded_file($imageTmpName, $imageDestination);

			if ($insertResult) {
				http_response_code(201);
				echo json_encode(['message' => "User added successfully"]);
			} else {
				http_response_code(503);
				echo json_encode(['message' => "Sorry! operation failed"]);
			}
		} else {
			echo "Image size too big!!!";
		}
	} else {
		echo "Error Occured";
	}
} else {
	echo "Image type is not allowed!!!";
}
