<?php

require_once './db.php';

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

$fetchQuery = "SELECT * from user";

$fetchResult = mysqli_query($connectResult, $fetchQuery);

$users = array();

while ($row = mysqli_fetch_assoc($fetchResult)) {
	$users[] = $row;
}

if ($users) {
	http_response_code(200);
	echo json_encode($users);
} else {
	http_response_code(404);
	echo json_encode(['message' => "User not found!!"]);
}
