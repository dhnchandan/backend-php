<?php

require_once './db.php';

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');

$id = isset($_GET['id']) ? $_GET['id'] : die();

$fetchQuery = "SELECT * from user where id = $id";

$fetchResult = mysqli_query($connectResult, $fetchQuery);

$user = array();

while ($row = mysqli_fetch_assoc($fetchResult)) {
	$user[] = $row;
}

if ($user) {
	http_response_code(200);
	echo json_encode($user[0]);
} else {
	http_response_code(404);
	echo json_encode(['message' => "User not found!!"]);
}
