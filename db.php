<?php

function connect()
{
	define('DB_SERVER', 'localhost');
	define('DB_USERNAME', 'root');
	define('DB_PASSWORD', '');
	define('DB_NAME', 'react_people');

	$connection = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
	return $connection;
}

$connectResult = connect();

if ($connectResult) {
	return true;
	echo "<h3>Database connect successfully!!</h3>";
} else {
	echo "<h3>Database connection failed!!</h3>";
	return false;
}
